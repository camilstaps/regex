module re

/**
 * Copyright 2016-2023 Camil Staps.
 *
 * This file is part of regex.
 *
 * Regex is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Regex is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with regex. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Error
import qualified Data.Map
import System.CommandLine

import Regex

instance <<< GroupId
where
	(<<<) f (NotNamed i) = f <<< "$" <<< (i+1)
	(<<<) f (Named n) = f <<< "$" <<< n

Start w
	# ([prog:opts],w) = getCommandLine w
	| length opts <> 2
		= abort ("Usage: "+++prog+++" REGEX INPUT\n")
	# rgx = hd opts
	# input = opts !! 1
	# rgx = compileRegex rgx
	| isError rgx
		= abort ("Error: "+++fromError rgx+++"\n")
	# matches = match (fromOk rgx) input
	# (io,w) = stdio w
	| isEmpty matches
		# io = io <<< "No match\n"
		# (_,w) = fclose io w
		= w
	# {start,end,groups} = hd matches
	# io = io <<< "$0\t(" <<< start <<< "," <<< end <<< ")\t" <<< (input % (start,end)) <<< "\n"
	# io = foldl
		(\io (g,(s,e)) -> io <<< g <<< "\t(" <<< s <<< "," <<< e <<< ")\t" <<< (input % (s,e)) <<< "\n")
		io
		('Data.Map'.toList groups)
	# (_,w) = fclose io w
	= w

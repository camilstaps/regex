module example

/**
 * Copyright 2016-2023 Camil Staps.
 *
 * This file is part of regex.
 *
 * Regex is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Regex is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with regex. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Error
import Data.Func
import Data.Functor
import qualified Data.Map

import Regex

Start = map display_match <$> flip match input <$> compileRegex rgx
where
	rgx = "^([a-zA-Z\\d][\\w\\.%+-]*)@((?:[a-zA-Z\\d-]+\\.)+[a-zA-Z]{2,})$"
	input = "test@example.org"

	display_match :: !Match -> (String, [(GroupId,String)])
	display_match {start,end,groups} =
		( input % (start,end)
		, [(g, input % range) \\ (g,range) <- 'Data.Map'.toList groups]
		)

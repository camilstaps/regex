definition module Regex

/**
 * Common definitions and imports to deal with regular expressions.
 *
 * Copyright 2016-2023 Camil Staps.
 *
 * This file is part of regex.
 *
 * Regex is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Regex is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with regex. If not, see <https://www.gnu.org/licenses/>.
 */

from StdOverloaded import class ==, class <

from Data.Error import :: MaybeError
from Data.Map import :: Map
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

import Regex.Parse
import Regex.Print

//* AST representation of a regex.
:: Regex
	= Literal !String
		//* Match a string exactly.
	| CharacterClass !{#Bool}
		//* The array contains one `Bool` for each character. Use
		//* {{`characterClass`}} and {{`invCharacterClass`}} to create a
		//* `CharacterClass` from a `[(Char,Char)]` listing the ranges.
	| Concat ![Regex]
		//* Concatenate regexes.
	| Any ![Regex]
		//* Match any of the regexes.
	| Repeated !Greediness !Int !(?Int) !Regex
		//* Repeated a regex at least `Int` times, with an optional maximum.
		//* Use {{`Many`}}, {{`Some`}}, and {{`Optional`}} for common cases.
	| Group !GroupId !Regex
		//* A matching subgroup.
	| Backreference !GroupId
		//* A backreference to an earlier matched subgroup.
	| StartOfString //* `^`
	| EndOfString   //* `$`
	| WordBreak     //* `\b`
	| NoWordBreak   //* `\B`

//* Used in {{`Repeated`}}.
:: Greediness = Greedy | Lazy

//* Identifier of a matching subgroup; see {{`Group`}}
:: GroupId = Named !String | NotNamed !Int
instance == GroupId
instance < GroupId

//* Bytecode for a regex.
:: CompiledRegex

derive JSONEncode CompiledRegex
derive JSONDecode CompiledRegex

/**
 * A match of a regex on a string.
 *
 * The `start` of the match is its first index; the `end` is the last index ---
 * inclusively; *not* the first non-matching index. Thus you can do
 * `s % (start,end)` to retrieve the full match.
 *
 * The matches of the `groups` work in the same way.
 *
 * Note that not all groups from the regex may be present in `groups`. Thus we
 * distinguish between empty submatches and unset submatches, something RE2
 * apparently does not do.
 */
:: Match =
	{ start  :: !Int
	, end    :: !Int
	, groups :: !Map GroupId (Int,Int)
	}

class regex r
where
	/**
	 * Compile a regex to bytecode representation. See {{`match`}} for why you
	 * would want to do that.
	 */
	compileRegex :: !r -> MaybeError String CompiledRegex

instance regex [Char], String, Regex, CompiledRegex

/**
 * Matches a string to a regex. Matches anywhere in the string can be returned;
 * wrap the regex in `^..$` to get only full matches.
 *
 * Typically, only the first match is used: the remaining matches contain not
 * only matches further in the string but also worse matches.
 *
 * With `r=Regex` and `r=CompiledRegex`, this is a total function. With
 * `r=String` and `r=[Char]`, this will `abort` on parse errors. To avoid this,
 * use `compileRegex` on the string representation first and catch the error
 * yourself.
 *
 * When a regex is used multiple times you should use `compileRegex` and use
 * the `CompiledRegex` for `match` to avoid doing double work.
 */
match :: !r !String -> [Match] | regex r
	special r=CompiledRegex; r=Regex; r=String; r=[Char]

//* Shorthand to define a `CharacterClass` from a list of ranges.
characterClass :: ![(Char,Char)] -> Regex

//* Like `characterClass`, but inverted.
invCharacterClass :: ![(Char,Char)] -> Regex

//* @type !Greediness -> Regex
Many g     :== Repeated g 0 ?None

//* @type !Greediness -> Regex
Some g     :== Repeated g 1 ?None

//* @type !Greediness -> Regex
Optional g :== Repeated g 0 (?Just 1)

/**
 * A string representation of the compiled regex, for debugging.
 * The result is a list of tuples of program counter and instruction.
 */
regexProgram :: !CompiledRegex -> [(Int, String)]

//* True for regexes that can match a string in linear time.
runsInLinearTime :: !CompiledRegex -> Bool

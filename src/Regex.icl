implementation module Regex

/**
 * Copyright 2016-2023 Camil Staps.
 *
 * This file is part of regex.
 *
 * Regex is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Regex is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with regex. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Monad
import Control.Monad.Fail
import Control.Monad.State
import Data.Func
import Data.Functor
import Data.Error
import Data.List
import qualified Data.Map
from Data.Map import :: Map
import qualified Data.Set
from Data.Map import :: Set
import System._Unsafe
import qualified Text
from Text import class Text, instance Text String, <+, concat3, concat4
import Text.GenJSON

import Regex.Parse
import Regex.Print

:: CompiledRegex =
	{ instructions      :: !{#Instr}
	, character_classes :: !{#{#Bool}}
	, groups            :: ![GroupId]
	}

derive JSONEncode CompiledRegex, Greediness, GroupId
derive JSONDecode CompiledRegex, Greediness, GroupId

JSONEncode{|{#Int}|} b is = JSONEncode{|*|} b [i \\ i <-: is]
JSONEncode{|{#{#Bool}}|} b classes = JSONEncode{|*|} b [[b \\ b <-: cls] \\ cls <-: classes]

JSONDecode{|{#Int}|} b json = case JSONDecode{|*|} b json of
	(?Just is, json) -> (?Just {i \\ i <- is}, json)
	(?None,    json) -> (?None, json)
JSONDecode{|{#{#Bool}}|} b json = case JSONDecode{|*|} b json of
	(?Just cs, json) -> (?Just {{b \\ b <- c} \\ c <- cs}, json)
	(?None,    json) -> (?None, json)

instance == {#Bool}
where
	(==) xs ys = size xs == size ys && and [x == y \\ x <-: xs & y <-: ys]

:: Instr :== Int

// Base instructions:
Ichar  :== 1 // `Ichar c` matches c
Ijmp   :== 2 // `Ijmp i` jumps to i
Isplit :== 3 // `Isplit i j` splits into two threads with pc i and j
Imatch :== 4 // ends successfully

// Groups:
Isave  :== 5 // `Isave i` saves the current index in location i

// Character classes:
Iclass :== 6 // `Iclass id` matches character class id

// Anchors:
Istart_of_string :==  7 // matches at index zero and after \n
Iend_of_string   :==  8 // matches at last character and before \n
Iword_break      :==  9 // between ^\w, \w$, \w\W, or \W\w (\w = [a-zA-Z0-9_]
Ino_word_break   :== 10 // the inverse of Iword_break

// Backreference:
Ibackreference :== 11 // jit-compiles bytecode to match the referenced group

// Temporary; used during compilation:
Ireserve :== -1

instance == GroupId
where
	(==) (Named a)    (Named b)    = a == b
	(==) (Named _)    (NotNamed _) = False
	(==) (NotNamed i) (NotNamed j) = i == j
	(==) (NotNamed _) (Named _)    = False

instance < GroupId
where
	(<) (NotNamed _) (Named _)    = True
	(<) (NotNamed i) (NotNamed j) = i < j
	(<) (Named _)    (NotNamed _) = False
	(<) (Named a)    (Named b)    = a < b

characterClass :: ![(Char,Char)] -> Regex
characterClass ranges = make_character_class False ranges

invCharacterClass :: ![(Char,Char)] -> Regex
invCharacterClass ranges = make_character_class True ranges

make_character_class :: !Bool ![(Char,Char)] -> Regex
make_character_class default ranges = CharacterClass $
	seqSt
		(\(f,t) map -> set (toInt f) (toInt t) map)
		ranges
		(createArray 256 default)
where
	set :: !Int !Int !*{#Bool} -> .{#Bool}
	set fr to arr
		| fr > to
			= arr
			= set (fr+1) to {arr & [fr]=not default}

instance regex [Char]
where
	compileRegex rgx = parseRegex rgx >>= compileRegex

instance regex String
where
	compileRegex rgx = compileRegex [c \\ c <-: rgx]

instance regex CompiledRegex
where
	compileRegex rgx = Ok rgx

:: CompileState =
	{ c_pc           :: !Int
	, c_instructions :: ![[Int]]
	, c_classes      :: ![{#Bool}]
	, c_groups       :: ![GroupId]
	}

instance regex Regex
where
	compileRegex rgx = finish <$> execStateT (compiler rgx)
		{ c_pc           = 9
		, c_instructions = [[Isplit,7,3,Iclass,0,Ijmp,0,Isave,0]]
		, c_classes      = [createArray 256 True]
		, c_groups       = []
		}
	where
		finish {c_instructions,c_classes,c_groups} =
			{ instructions      = {i \\ i <- flatten $ reverse [[Isave,1,Imatch]:c_instructions]}
			, character_classes = {cls \\ cls <- reverse c_classes}
			, groups            = reverse c_groups
			}

get_pc :: StateT CompileState m Int | Monad m
get_pc = gets \s -> s.c_pc

//* Add instructions to the compiled regex.
add :: ![Instr] -> StateT CompileState m Int | Monad m
add instrs =
	modify (\s=:{c_pc,c_instructions} ->
		{ s
		& c_pc           = c_pc+length instrs
		, c_instructions = [instrs:c_instructions]
		}) >>|
	get_pc

//* Fill previously added `Ireserve` slots.
fill_reserved_slots :: ![Int] -> StateT CompileState m () | Monad m
fill_reserved_slots is = modify \st -> {st & c_instructions=fill is st.c_instructions}
where
	fill [] blocks = blocks
	fill fs [block:blocks]
		# (fs,block) = fill_block fs block
		= [block:fill fs blocks]
	where
		fill_block :: ![Int] ![Int] -> (![Int], ![Int])
		fill_block [] instrs = ([], instrs)
		fill_block fs [] = (fs, [])
		fill_block allfs=:[f:fs] [i:is]
			| i == Ireserve
				# (fs,is) = fill_block fs is
				= (fs,[f:is])
				# (fs,is) = fill_block allfs is
				= (fs,[i:is])

//* The actual compiler.
compiler :: !Regex -> StateT CompileState (MaybeError String) Int
compiler (Literal cs) =
	add $ flatten [[Ichar,toInt c] \\ c <-: cs]
compiler (CharacterClass cls) =
	gets (\s -> s.c_classes) >>= \clss ->
	case elemIndex cls clss of
		?Just id ->
			add [Iclass,length clss-id-1]
		?None ->
			modify (\s -> {s & c_classes=[cls:clss]}) >>|
			add [Iclass,length clss]
compiler (Concat rs) =
	mapM_ compiler rs >>|
	get_pc
compiler (Any rs) =
	c rs
where
	c [r] = compiler r
	c [r:rs] =
		add [Isplit,Ireserve,Ireserve] >>= \pc1 ->
		compiler r >>|
		add [Ijmp,Ireserve] >>= \pc2 ->
		c rs >>= \pc3 ->
		fill_reserved_slots [pc3] >>| // jmp
		fill_reserved_slots [pc1,pc2] >>| // split
		get_pc
compiler (Repeated greedy min mbMax r) =
	sequence_ [compiler r \\ _ <- [1..min]] >>|
	case mbMax of
		?None ->
			add [Isplit,Ireserve,Ireserve] >>= \pc2 ->
			compiler r >>= \pc3 ->
			fill_reserved_slots (if greedy=:Greedy [pc2,pc3+2] [pc3+2,pc2]) >>| // split
			add [Ijmp,pc2-3]
		?Just max ->
			compiler (make_body (max-min) (Literal ""))
where
	make_body :: !Int !Regex -> Regex
	make_body 0 body = body
	make_body n body = make_body (n-1) $ Concat
		[ body
		, Any (if greedy=:Greedy [r,Literal ""] [Literal "",r])
		]
compiler (Group id r) =
	modify (\s -> {s & c_groups=[id:s.c_groups]}) >>|
	gets (\s -> length s.c_groups) >>= \id ->
	add [Isave,2*id] >>|
	compiler r >>|
	add [Isave,2*id+1]
compiler (Backreference id) =
	gets (\s -> s.c_groups) >>= \groups ->
	case elemIndex id groups of
		?None -> fail (concat3 "unknown group " (groupIdToString id) " in backreference")
		?Just id -> add [Ibackreference,2*(length groups-id)]
where
	groupIdToString (Named n) = n
	groupIdToString (NotNamed i) = toString (i+1)
compiler StartOfString =
	add [Istart_of_string]
compiler EndOfString =
	add [Iend_of_string]
compiler WordBreak =
	add [Iword_break]
compiler NoWordBreak =
	add [Ino_word_break]

//* State of a thread during regex execution.
:: ThreadState =
	{ pc                 :: !Int
	, locations          :: !.{#Int}
	, backreference_code :: !{#Int} //* jit-compiled bytecode for a backreference
	}

//* Program counters higher than this reference the `backreference_code` in a `ThreadState`.
BACKREFERENCE_JIT_OFFSET :== 0x1000000

:: SeenKey :== {#Int}

instance < SeenKey
where
	(<) xs ys
		| size xs < size ys = True
		| size ys < size xs = False
		| otherwise = lt 0
	where
		lt i
			| i >= size xs = False
			| xs.[i] < ys.[i] = True
			| ys.[i] < xs.[i] = False
			| otherwise = lt (i+1)

//* Overloaded wrapper around match`.
match :: !r !String -> [Match] | regex r
match rgx s = case compileRegex rgx of
	Ok rgx  -> match` rgx s
	Error e -> abort ('Text'.concat ["regex failed to parse: ",e,"\n"])

//* Run a compiled regex on a string.
match` :: !CompiledRegex !String -> [Match]
match` {instructions=r,character_classes,groups} s
	# matches = run 0 [{pc=0, locations=createArray (2*(length groups+1)) -1, backreference_code={}}]
	= map finalize matches
where
	//* Create a `Match` object from a locations array.
	finalize :: !{#Int} -> Match
	finalize locs =
		{ start  = locs.[0]
		, end    = locs.[1]-1
		, groups = 'Data.Map'.fromList (get_groups 2 groups)
		}
	where
		get_groups _ [] = []
		get_groups i [g:gs]
			# rest = get_groups (i+2) gs
			| locs.[i]   < 0 = rest
			| locs.[i+1] < 0 = rest
			| otherwise      = [(g,(locs.[i],locs.[i+1]-1)):rest]

	//* The indexes of locations used by backreferences, which are needed to
	//* produce a `SeenKey`.
	backreferences = findBackreferences 0
	where
		findBackreferences :: !Int -> [Int]
		findBackreferences i
			| i >= size r
				= []
			# instr = r.[i]
			| instr==Imatch || instr==Istart_of_string || instr==Iend_of_string ||
					instr==Iword_break || instr==Ino_word_break
				= findBackreferences (i+1)
			| instr==Ichar || instr==Ijmp || instr==Isave || instr==Iclass
				= findBackreferences (i+2)
			| instr==Isplit
				= findBackreferences (i+3)
			| instr==Ibackreference
				= [r.[i+1], r.[i+1]+1 : findBackreferences (i+2)]

	//* A `SeenKey` contains the information to determine whether states should
	//* be pruned. States are equivalent when they have the same program
	//* counter (so we use a `{Set SeenKey}` indexed by program counter) and
	//* when the stored locations used by backreferences are identical.
	seenKey :: !*ThreadState -> (!SeenKey, !*ThreadState)
	seenKey t=:{locations} = ({locations.[i] \\ i <- backreferences}, t)

	//* Iteratively run the bytecode.
	run :: !Int ![*ThreadState] -> [{#Int}]
	run i threads
		# threads = run_threads (createArray (size r) 'Data.Set'.newSet) threads
		| i >= size s
			= [locations \\ {pc,locations} <- threads | pc < BACKREFERENCE_JIT_OFFSET && r.[pc] == Imatch]
			# threads = prune_threads threads
			= run (i+1) threads
	where
		//* Check whether threads can be pruned; this performs the actual
		//* matching against input characters.
		prune_threads :: ![*ThreadState] -> [*ThreadState]
		prune_threads [] = []
		prune_threads [t=:{pc}:ts]
			# rest = prune_threads ts
			# instr = if in_jit_code t.backreference_code.[pc-BACKREFERENCE_JIT_OFFSET] r.[pc]
			| instr == Ichar
				| toChar (if in_jit_code t.backreference_code.[pc-BACKREFERENCE_JIT_OFFSET+1] r.[pc+1]) == s.[i]
					= [{t & pc=pc+2}:rest]
					= rest
			| instr == Iclass
				| character_classes.[r.[pc+1]].[toInt s.[i]]
					= [{t & pc=pc+2}:rest]
					= rest
			| otherwise
				= [t:rest]
		where
			in_jit_code = pc >= BACKREFERENCE_JIT_OFFSET

		//* Run until we need new input. This ensures that threads are created
		//* with the right priority.
		run_threads :: !*{!Set SeenKey} ![*ThreadState] -> [*ThreadState]
		run_threads _ [] = []
		run_threads seen [t=:{pc,backreference_code}:ts]
			# (this_seen,seen) = if in_jit_code ('Data.Set'.newSet, seen) seen![pc]
			# (key,t) = seenKey t
			| 'Data.Set'.member key this_seen
				= run_threads seen ts
			# instr = if in_jit_code backreference_code.[pc-BACKREFERENCE_JIT_OFFSET] r.[pc]
			# seen = if (in_jit_code || instr == Imatch) seen {seen & [pc] = 'Data.Set'.insert key this_seen}
			| instr == Ichar || instr == Iclass || instr == Imatch
				= [t:run_threads seen ts]
			| instr == Ijmp
				| in_jit_code
					= run_threads seen [{t & pc=backreference_code.[pc-BACKREFERENCE_JIT_OFFSET+1]}:ts]
					= run_threads seen [{t & pc=r.[pc+1]}:ts]
			| instr == Isplit
				# (new_locations,t) = extract_locations t
				# pc1 = r.[pc+1]
				# pc2 = r.[pc+2]
				= run_threads seen [{t & pc=pc1},{pc=pc2,locations=new_locations,backreference_code={}}:ts]
			| instr == Istart_of_string
				| i == 0
					= run_threads seen [{t & pc=pc+1}:ts]
					= run_threads seen ts
			| instr == Iend_of_string
				| i == size s || (i == size s-1 && s.[i] == '\n')
					= run_threads seen [{t & pc=pc+1}:ts]
					= run_threads seen ts
			| instr == Iword_break
				| is_at_word_break
					= run_threads seen [{t & pc=pc+1}:ts]
					= run_threads seen ts
			| instr == Ino_word_break
				| is_at_word_break
					= run_threads seen ts
					= run_threads seen [{t & pc=pc+1}:ts]
			| instr == Isave
				# t & locations.[r.[pc+1]] = i
				= run_threads seen [{t & pc=pc+2}:ts]
			| instr == Ibackreference
				# (start,t) = t!locations.[r.[pc+1]]
				  (end,t) = t!locations.[r.[pc+1]+1]
				| start < 0 || end < 0 // the group that is backreferenced has not been matched yet
					= run_threads seen ts
				# t & backreference_code = {i \\ i <- flatten [[Ichar,toInt s.[i]] \\ i <- [start..end-1]] ++ [Ijmp,pc+2]}
				  t & pc = BACKREFERENCE_JIT_OFFSET
				= run_threads seen [t:ts]
			| otherwise
				= abort ("Regex.match: illegal instruction "+++toString instr+++"\n")
		where
			in_jit_code = pc >= BACKREFERENCE_JIT_OFFSET

			//* Copy the locations array in case of a `Isplit`.
			extract_locations :: !*ThreadState -> *(!*{#Int}, !*ThreadState)
			extract_locations t=:{locations}
				# (sz,locations) = usize locations
				# (cpy,locations) = copy (sz-1) (createArray sz -1) locations
				= (cpy, {t & locations=locations})
			where
				copy i to fr
					| i < 0 = (to,fr)
					# (x,fr) = fr![i]
					# to & [i] = x
					= copy (i-1) to fr

			is_at_word_break
				| size s == 0
					= False
				| i == 0
					= is_word_char s.[0]
				| i == size s
					= is_word_char s.[i-1]
					= is_word_char s.[i-1] <> is_word_char s.[i]
			where
				is_word_char c = isAlphanum c || c == '_'

regexProgram :: !CompiledRegex -> [(Int, String)]
regexProgram r = print 0 [i \\ i <-: r.instructions]
where
	print _ [] = []
	print pc [Ichar,c:is]
		= [(pc, concat3 "char\t'" (escapeForRegex (toChar c)) "'") : print (pc+2) is]
	print pc [Ijmp,i:is]
		= [(pc, "jmp\t" <+ toString i) : print (pc+2) is]
	print pc [Isplit,i1,i2:is]
		= [(pc, concat4 "split\t" (toString i1) " " (toString i2)) : print (pc+3) is]
	print pc [Imatch:is]
		= [(pc, "match") : print (pc+1) is]
	print pc [Isave,i:is]
		= [(pc, "save\t" <+ i) : print (pc+2) is]
	print pc [Iclass,i:is]
		= [(pc, "class\t" <+ i) : print (pc+2) is]
	print pc [Istart_of_string:is]
		= [(pc, "^") : print (pc+1) is]
	print pc [Iend_of_string:is]
		= [(pc, "$") : print (pc+1) is]
	print pc [Iword_break:is]
		= [(pc, "\\b") : print (pc+1) is]
	print pc [Ino_word_break:is]
		= [(pc, "\\B") : print (pc+1) is]
	print pc [Ibackreference,i:is]
		= [(pc, "ref\t" <+ i) : print (pc+2) is]
	print pc [i:is]
		= abort ("regexProgram: unexpected instruction " <+ i <+ "\n")

runsInLinearTime :: !CompiledRegex -> Bool
runsInLinearTime {instructions} = check 0
where
	check :: !Int -> Bool
	check i
		| i >= size instructions
			= True
		# instr = instructions.[i]
		| instr==Imatch || instr==Istart_of_string || instr==Iend_of_string ||
				instr==Iword_break || instr==Ino_word_break
			= check (i+1)
		| instr==Ichar || instr==Ijmp || instr==Isave || instr==Iclass
			= check (i+2)
		| instr==Isplit
			= check (i+3)
		| instr==Ibackreference
			= False

implementation module Regex.Util

/**
 * Copyright 2016-2023 Camil Staps.
 *
 * This file is part of regex.
 *
 * Regex is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Regex is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with regex. If not, see <https://www.gnu.org/licenses/>.
 */

escape_sequences :: [(Char, Char)]
escape_sequences =:
	[ ('a', '\x07')
	, ('e', '\x1b')
	, ('f', '\x0c')
	, ('n', '\x0a')
	, ('r', '\x0d')
	, ('t', '\x09')
	, ('v', '\x0b')
	, ('\\', '\\')
	]

shorthand_classes :: [(Char, [(Char, Char)])]
shorthand_classes =:
	[ ('w', [('A','Z'),('a','z'),('0','9'),('_','_')])
	, ('W', [('\0','/'),(':','@'),('[','^'),('`','`'),('{','\xff')])
	, ('d', [('0','9')])
	, ('D', [('\x00','/'),(':','\xff')])
	, ('s', [(' ',' '),('\t','\n'),('\x0c','\x0d')])
	, ('S', [('\x00','\x08'),('\x0b','\x0b'),('\x0e','\x1f'),('!','\xff')])
	]

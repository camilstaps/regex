implementation module Regex.Print

/**
 * Copyright 2016-2023 Camil Staps.
 *
 * This file is part of regex.
 *
 * Regex is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Regex is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with regex. If not, see <https://www.gnu.org/licenses/>.
 */

import _SystemArray
import _SystemStrictLists
import StdBool
from StdFunc import flip
import StdString
import StdTuple

from Data.Func import $
import Data.List
import Data.Maybe
from Text import class Text(concat), instance Text String, instance + String

import Regex
import Regex.Util

print :: Bool Regex -> String
print ps (Literal cs) = parens (ps && size cs > 1) $ concat [escapeForRegex c \\ c <-: cs]
print ps (CharacterClass map) = concat ["[":ranges 255++["]"]]
where
	ranges -1 = []
	ranges i
		| not map.[i]
			= ranges (i-1)
			= find_range i (i-1)
	find_range end -1 = [escapeForRegex '\0',"-",escapeForRegex (toChar end)]
	find_range end i
		| map.[i]
			= find_range end (i-1)
			# range = if (i+1 == end)
				[escapeForRegex (toChar end)]
				[escapeForRegex (toChar (i+1)),"-",escapeForRegex (toChar end)]
			= ranges (i-1) ++ range
print ps (Concat rgxs) = parens ps $ foldl (+) r rs
where [r:rs] = map (print True) rgxs
print ps (Any rgxs) = parens ps $ foldl (\x s -> x + "|" + s) r rs
where [r:rs] = map (print True) rgxs
print ps (Repeated g 0 (?Just 1) r) = print True r <+ "?" <+ lz g
print ps (Repeated g 1 (?Just 1) r) = print True r
print ps (Repeated g 0 ?None     r) = print True r <+ "*" <+ lz g
print ps (Repeated g 1 ?None     r) = print True r <+ "+" <+ lz g
print ps (Repeated g f t r) = print True r <+ q <+ lz g
where
	q
	| isNone t  = "{" <+ f <+ ",}"
	| f == jt   = "{" <+ f <+ "}"
	| otherwise = "{" <+ f <+ "," <+ jt <+ "}"
	where jt = fromJust t
print ps (Group (NotNamed _) r) = "(" <+ r <+ ")"
print ps (Group (Named n) r) = "(?'" <+ n <+ "'" <+ r <+ ")" // NB: no parsing support yet
print ps (Backreference (NotNamed i)) = "\\g" <+ (i+1)
print ps (Backreference (Named n)) = "\\g{" <+ n <+ "}"
print ps StartOfString = "^"
print ps EndOfString = "$"
print ps WordBreak = "\\b"
print ps NoWordBreak = "\\B"

lz :: !Greediness -> String
lz g = if (g=:Greedy) "" "?"

instance toString Regex where toString r = print False r

parens :: Bool a -> String | toString a
parens True  s = "(?:" <+ s <+ ")"
parens False s = toString s

(<+) infixr 5 :: a b -> String | toString a & toString b
(<+) x y = toString x + toString y

escapeForRegex :: !Char -> String
escapeForRegex '.' = "\\."
escapeForRegex c
| isMember c $ map snd escape_sequences
	= hd [{'\\',n} \\ (n,c`) <- escape_sequences | c == c`]
| c < ' ' || c > '~'
	= {'\\', 'x', hex (toInt c / 16), hex (toInt c rem 16)}
| otherwise
	= {c}
where
	hex i = "0123456789abcdef".[i]

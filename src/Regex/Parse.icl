implementation module Regex.Parse

/**
 * Copyright 2016-2023 Camil Staps.
 *
 * This file is part of regex.
 *
 * Regex is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Regex is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with regex. If not, see <https://www.gnu.org/licenses/>.
 */

import _SystemArray
import _SystemStrictLists
import StdBool
from StdFunc import o, flip
import StdList
import StdString
import StdTuple

import Control.Applicative
import Control.Monad
import Data.Error
from Data.Func import $
import Data.Functor
import Data.List
import Data.Maybe
import Data.Tuple
import qualified Text
from Text import class Text, instance Text String

import qualified Regex
import Regex
import Regex.Util

instance zero String where zero = ""

instance Alternative (MaybeError a) | zero a
where
	empty = Error zero

	(<|>) (Error _) r = r
	(<|>) l         _ = l

Concat rs = 'Regex'.Concat $ concat rs
where
	concat [r=:(Literal _):rs] = [Literal $ 'Text'.concat [l \\ Literal l <- [r:lits]]:concat rest]
	where
		(lits,rest) = span (\r -> r=:(Literal _)) rs
	concat [r:rs] = [r:concat rs]
	concat [] = []

parseRegex :: ![Char] -> MaybeError String Regex
parseRegex cs = compile 0 [] cs
where
	compile :: Int [Regex] [Char] -> MaybeErrorString Regex
	compile _ []  [] = Ok $ Concat []
	compile _ [r] [] = Ok r
	compile _ rs  [] = Ok $ Concat $ reverse rs
	compile g rs  cs = parse g rs cs >>= uncurry3 compile

parse :: Int [Regex] [Char] -> MaybeErrorString (Int, [Regex], [Char])
parse g rs [] = Ok (g, rs, [])
parse g [r:rs] ['+':'?':cs] = Ok (g, [Some Lazy       r:rs], cs)
parse g [r:rs] ['+':cs]     = Ok (g, [Some Greedy     r:rs], cs)
parse g [r:rs] ['*':'?':cs] = Ok (g, [Many Lazy       r:rs], cs)
parse g [r:rs] ['*':cs]     = Ok (g, [Many Greedy     r:rs], cs)
parse g [r:rs] ['?':'?':cs] = Ok (g, [Optional Lazy   r:rs], cs)
parse g [r:rs] ['?':cs]     = Ok (g, [Optional Greedy r:rs], cs)

parse g rs ['|':cs]
	= (\(g,rs`,cs) -> (g,[Any [revConcat rs:rs`]],cs)) <$> alternatives g [] cs
where
	alternatives :: Int [Regex] [Char] -> MaybeErrorString (Int, [Regex], [Char])
	alternatives g rs []
		= Ok (g, [revConcat rs], [])
	alternatives g rs cs=:[')':_]
		= Ok (g, [revConcat rs], cs)
	alternatives g rs ['|':cs]
		= (\(g,rs`,cs) -> (g, [revConcat rs:rs`], cs)) <$> alternatives g [] cs
	alternatives g rs cs
		= parse g rs cs >>= uncurry3 alternatives

parse g [r:rs] ['{':cs]
| isNone fr = Error "couldn't parse from part of quantifier"
| hd cs` == '}'
	| length cs` > 1 && cs`!!1 == '?'
		= Ok (g, [Repeated Lazy   fr` (?Just fr`) r:rs], drop 2 cs`)
		= Ok (g, [Repeated Greedy fr` (?Just fr`) r:rs], tl cs`)
| isNone to = Error "couldn't parse to part of quantifier"
| hd cs` == ',' && hd cs`` == '}'
	| length cs`` > 1 && cs``!!1 == '?'
		= Ok (g, [Repeated Lazy   fr` to` r:rs], drop 2 cs``)
		= Ok (g, [Repeated Greedy fr` to` r:rs], tl cs``)
| otherwise = Error "couldn't parse quantifier"
where
	(fr`, to`) = (fromJust fr, fromJust to)
	(fr, cs`)  = appFst parseInt $ span isDigit cs
	(to, cs``) = appFst toEnd $ span isDigit $ tl cs`

	parseInt :: [Char] -> ?Int
	parseInt [] = ?None
	parseInt cs = if (all isDigit cs) (?Just $ toInt $ toString $ cs) ?None

	toEnd :: [Char] -> ?(?Int)
	toEnd [] = ?Just ?None
	toEnd cs = ?Just <$> parseInt cs

parse g rs ['[':'^':cs]
	= (\(cc,cs) -> (g, [invCharacterClass cc:rs], cs)) <$> charClass [] cs
parse g rs ['[':cs]
	= (\(cc,cs) -> (g, [characterClass cc:rs], cs)) <$> charClass [] cs

parse g rs ['(':'?':':':cs] = app <$> inGroup g [] cs
where
	app (g, [],  cs) = (g, rs, cs)
	app (g, [r], cs) = (g, [r:rs], cs)
	app (g, rs`, cs) = (g, [Concat $ reverse rs`:rs], cs)

parse g rs ['(':cs] = (\(g`,rs`,cs) -> (g`, [Group (NotNamed g) (cc rs`):rs], cs)) <$> inGroup (g+1) [] cs
where
	cc [r] = r; cc rs = Concat $ reverse rs

parse g rs cs
	= (\(c,cs) -> (g, [characterClass c:rs],cs)) <$> shorthandClass cs
		<|> (\(a,cs) -> (g, [a:rs], cs)) <$> anchor cs
		<|> (\(id,cs) -> (g, [Backreference id:rs], cs)) <$> backreference cs
		<|> (\(c,cs) -> (g, [Literal {c}:rs], cs)) <$> singleChar cs
		<|> Error ("Cannot parse " +++ toString cs)

inGroup :: Int [Regex] [Char] -> MaybeErrorString (Int, [Regex], [Char])
inGroup g grp [] = Error "unclosed group"
inGroup g grp [')':cs] = Ok (g, grp, cs)
inGroup g grp cs = parse g grp cs >>= uncurry3 inGroup

anchor :: [Char] -> MaybeErrorString (Regex, [Char])
anchor ['^':cs] = Ok (StartOfString, cs)
anchor ['$':cs] = Ok (EndOfString, cs)
anchor ['\\':'b':cs] = Ok (WordBreak, cs)
anchor ['\\':'B':cs] = Ok (NoWordBreak, cs)
anchor _ = Error "expected anchor"

singleChar :: [Char] -> MaybeErrorString (Char, [Char])
singleChar ['\\':'x':'{':c1:c2:c3:c4:'}':cs]
| isHexDigit c1 && isHexDigit c2 && isHexDigit c3 && isHexDigit c4
	= Ok (toChar $ (fromHex c1 << 12) + (fromHex c2 << 8) + (fromHex c3 << 4) + fromHex c4, cs)
	= Error "invalid hexadecimal escape sequence"
singleChar ['\\':'x':c1:c2:cs]
| isHexDigit c1 && isHexDigit c2
	= Ok (toChar $ (fromHex c1 << 4) + fromHex c2, cs)
	= Error "invalid hexadecimal escape sequence"
singleChar ['\\':c:cs]
| isMember c escapable
	= Ok (c, cs)
| isMember c $ map fst escape_sequences
	= Ok (snd $ hd $ filter ((==) c o fst) escape_sequences, cs)
| isOctDigit c
	= Ok (toChar $ foldl ((+) o ((*) 8)) 0 $ map digitToInt octs, nonocts)
| otherwise
	 = Error "expected character"
where
	escapable = ['\\+*?()[]{}^$.|']

	(octs,nonocts) = spanMax 3 isOctDigit [c:cs]

	spanMax :: Int (a -> Bool) [a] -> ([a], [a])
	spanMax 0 _ xs     = ([], xs)
	spanMax _ _ []     = ([], [])
	spanMax n f [x:xs] = if (f x) ([x:ys],zs) ([],[x:xs])
	where (ys,zs) = spanMax (n-1) f xs
singleChar [c:cs]
	= Ok (c,cs)
singleChar []
	= Error "expected character"

fromHex :: !Char -> Int
fromHex c
	| c < 'A'
		= toInt (c - '0')
	| c < 'a'
		= toInt (c - 'A') + 10
		= toInt (c - 'a') + 10

backreference :: [Char] -> MaybeErrorString (GroupId, [Char])
backreference ['\\':cs] = case cs of
	['g':cs=:[c:_]] | isDigit c && c <> '0' -> succeed cs
	[c1,c2:_] | isDigit c1 && c1 <> '0' && not (isDigit c2) -> succeed cs
	[c1] | isDigit c1 && c1 <> '0' -> succeed cs
	_ -> Error "expected backreference"
where
	succeed cs = Ok (NotNamed (toInt {#c \\ c <- id} - 1), rest)
	where
		(id,rest) = span isDigit cs
backreference _ = Error "expected backreference"

charClass :: [(Char,Char)] [Char] -> MaybeErrorString ([(Char,Char)], [Char])
charClass _ [] = Error "unclosed character class"
charClass cls=:[_:_] [']':cs] = Ok (cls,cs)
charClass cls cs = (literal_dot cs <|> shorthandClass cs <|> try_chars cs) >>=
	\(cls`,cs`) -> charClass (cls ++ cls`) cs`
where
	literal_dot ['.':cs] = Ok ([('.','.')],cs)
	literal_dot cs = Error "expected ."

	try_chars cs = (singleChar cs <|> other_escape cs) >>= \(c1,cs`) -> case cs` of
		['-':']':cs``] = Ok ([(c1,c1),('-','-')], [']':cs``])
		['-':cs``]     = (\(c2,cs) -> ([(c1,c2)],cs)) <$> singleChar cs``
		_              = Ok ([(c1,c1)], cs`)

	// In character classes, unrecognized escapes are fine (and sometimes used,
	// e.g. `\-`). PCRE accepts \b here as BACKSPACE (not word boundary), while
	// POSIX does not. RE2 disallows \b in character classes, which is what we
	// follow here. See https://swtch.com/~rsc/regexp/regexp3.html#caveats.
	other_escape ['\\':c:cs]
		| c == 'b' = Error "\\b not allowed in character classes"
		| otherwise = Ok (c,cs)
	other_escape cs = Error "expected \\"

shorthandClass :: [Char] -> MaybeErrorString ([(Char,Char)], [Char])
shorthandClass ['\\':c:cs] = case lookup c shorthand_classes of
	?None    -> Error $ "unknown shorthand class \\" +++ {c}
	?Just cc -> Ok (cc,cs)
shorthandClass ['.':cs] = Ok ([('\x00','\x09'),('\x0b','\xff')], cs)
shorthandClass _ = Error "expected character class"

revConcat :: [Regex] -> Regex
revConcat [r] = r
revConcat rs = Concat $ reverse rs

uncurry3 :: (a b c -> d) (a,b,c) -> d
uncurry3 f (x,y,z) = f x y z

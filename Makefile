CLM:=clm
CLMFLAGS:=-IL Platform -tst -h 50m -nt

EXE:=example re test

all: $(EXE)

run_test: test
	cleantest --hide start --junit junit.xml -r ./$< -O -nr

$(EXE): .FORCE
	$(CLM) $(CLMFLAGS) $@ -o $@

clean:
	$(RM) -r Clean\ System\ Files $(EXE)

.PHONY: all clean run_test .FORCE

.FORCE:

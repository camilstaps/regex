# Changelog

#### v2.1.2

- Chore: allow `base` `3`.

#### v2.1.1

- Chore: allow containers 2, json 2 and 3, and text 2.

### v2.1

- Chore: update to base 2.0.

#### v2.0.1

- Fix: add missing alternative for backreferences in pretty-printer.

## v2.0.0

- Feature: add support for backreferences, e.g. `(a|b)\1` or `(a|b)\g1`. These
  are not guaranteed to run in linear time or space.
- Feature: add function `runsInLinearTime` to test if a regex can be matched in
  time linear to the input.
- Change: octal escapes other than `\0` now must have at least two characters;
  `\1` through `\9` are recognized as backreferences.

#### v1.0.1

- Chore: move to https://clean-lang.org.

## v1.0.0

First tagged version.

module test

/**
 * Copyright 2016-2021 Camil Staps.
 *
 * This file is part of regex.
 *
 * Regex is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Regex is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with regex. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Control.Monad
import Control.Monad.Identity
import Control.Monad.State
import Data.Error
import Data.Functor
import qualified Data.Map
import Data.Maybe
import System._Unsafe
import Testing.TestEvents
from Text import class Text(concat), instance Text String
import Text.GenJSON
import Text.GenPrint

import Regex

Start w
	# (ok,f,w) = sfopen "tests" FReadText w
	| not ok
		= abort "Failed to open test data\n"
	# (io,w) = stdio w
	# io = run_tests (read 0 f) io
	# (_,w) = fclose io w
	= w

:: TestSuite :== [TestInput]

:: TestInput
	= TRegex !Bool /* skip? */ !String
	| TInput !String
	| TMatch !Int !String
	| TNoMatch

:: TestState =
	{ cur_names    :: ![String]
	, cur_location :: !TestLocation
	, cur_skipped  :: !Bool
	, cur_regex    :: !?CompiledRegex
	, cur_input    :: !String
	, cur_matches  :: ![Match]
	, cur_fails    :: ![(String,?FailReason)]
	, io           :: !File
	}

tell :: !TestEvent -> State TestState ()
tell ev = modify
	\st=:{cur_fails,io}
		# io = unsafeCoerce io <<< toJSON ev <<< "\n"
		->
			{ st
			& cur_fails = case ev of
				EndEvent {name,event=Failed r}
					-> [(name,r):cur_fails]
					-> cur_fails
			, io = unsafeCoerce io
			}

// Finish previous test: send Passed event if applicable
wrap_up :: State TestState ()
wrap_up =
	gets id >>= \{cur_names,cur_location,cur_skipped,cur_regex,cur_fails} ->
	case cur_regex of
		?Just _ | not cur_skipped ->
			let
				(event,msg) = case cur_fails of
					[] -> (Passed, "")
					_  -> (Failed (?Just (FailedChildren (reverse cur_fails))), "failures")
				end = EndEvent
					{ emptyEndEvent
					& name     = last cur_names
					, location = ?Just cur_location
					, event    = event
					, message  = msg
					}
			in
			tell end
		_ ->
			pure ()

run_tests :: ![(Int,TestInput)] !*File -> *File
run_tests input io = unsafeCoerce (evalState tests)
	{ cur_names    = []
	, cur_location = {moduleName= ?None}
	, cur_skipped  = False
	, cur_regex    = ?None
	, cur_input    = ""
	, cur_matches  = []
	, cur_fails    = []
	, io           = unsafeCoerce io
	}
where
	tests =
		mapM_ (uncurry run_test_input) input >>|
		wrap_up >>|
		gets \st -> st.io

run_test_input :: !Int !TestInput -> State TestState ()
run_test_input lineno (TRegex skip rgx) =
	wrap_up >>|
	let
		name = concat ["/",escape rgx,"/"]
		location = {moduleName= ?Just ("tests:"+++toString lineno)}
	in
	modify (\st -> {st & cur_names=[name], cur_location=location, cur_skipped=skip}) >>|
	tell (StartEvent {StartEvent | name=name, location= ?Just location}) >>|
	if skip
	(tell (EndEvent {emptyEndEvent & name=name, location= ?Just location, event=Skipped}))
	(case compileRegex rgx of
		Ok rgx ->
			modify (\st -> {st & cur_regex= ?Just rgx, cur_fails=[]})
		Error err ->
			let
				msg = "Compilation failed: "+++err
				end = EndEvent
					{ emptyEndEvent
					& name     = name
					, location = ?Just location
					, event    = Failed (?Just (CustomFailReason msg))
					, message  = msg
					}
			in
			modify (\st -> {st & cur_names=[name], cur_regex= ?None}) >>|
			tell end)
run_test_input _ (TInput s) =
	gets id >>= \{cur_names=names,cur_location,cur_skipped,cur_regex}
	| cur_skipped ->
		pure ()
	| otherwise -> case cur_regex of
		?None ->
			pure ()
		?Just rgx ->
			let my_name = concat [last names,": ",escape s] in
			modify (\st ->
				{ st
				& cur_names   = [my_name,last names]
				, cur_input   = s
				, cur_matches = match rgx s
				}) >>|
			tell (StartEvent {StartEvent | name=my_name, location= ?Just cur_location})
run_test_input _ (TMatch i s) =
	gets id >>= \{cur_names=names=:[name:_],cur_location,cur_skipped,cur_regex,cur_input,cur_matches}
	| cur_skipped || isNone cur_regex ->
		pure ()
	// TODO: this generates multiple Failed events for the same input
	| otherwise -> case cur_matches of
		[]
			# msg = "expected match"
			# end = EndEvent
				{ emptyEndEvent
				& name     = name
				, location = ?Just cur_location
				, event    = Failed (?Just (CustomFailReason msg))
				, message  = msg
				}
			-> tell end
		[{start,end,groups}:_]
			# match = if (i==0)
				(cur_input % (start,end))
				(maybe "<unset>" ((%) cur_input) ('Data.Map'.get (NotNamed (i-1)) groups))
			# (event,msg) = if (match == s)
				(Passed, "")
				(Failed (?Just (FailedAssertions
					[ExpectedRelation (GPrint (printToString s)) Eq (GPrint (printToString match))]))
				, "match failure"
				)
			# end = EndEvent
				{ emptyEndEvent
				& name     = name
				, location = ?Just cur_location
				, event    = event
				, message  = msg
				}
			-> tell end
run_test_input _ TNoMatch =
	gets id >>= \{cur_names=names=:[name:_],cur_location,cur_skipped,cur_regex,cur_input,cur_matches}
	| cur_skipped || isNone cur_regex ->
		pure ()
	| otherwise -> case cur_matches of
		[]
			# end = EndEvent
				{ emptyEndEvent
				& name     = name
				, location = ?Just cur_location
				, event    = Passed
				}
			-> tell end
		[{start,end}:_]
			# end = EndEvent
				{ emptyEndEvent
				& name     = name
				, location = ?Just cur_location
				, event    = Failed (?Just (CustomFailReason
					(concat ["expected no match; got (",toString start,",",toString end,")"])))
				, message  = "expected no match"
				}
			-> tell end

read :: !Int !File -> [(Int,TestInput)]
read lineno f
	| sfend f
		= []
	# (ln,f) = sfreadline f
	# lineno = lineno + 1
	| size ln == 1 && ln.[0] == '\n' // empty line
		= read lineno f
	| ln.[0] == '#' // comment
		= read lineno f
	| ln.[0] == '/' // regex
		// TODO: flags
		= [(lineno,TRegex False (ln % (1,size ln-3))):read lineno f]
	| ln.[0] == 's' && ln.[1] == 'k' && ln.[2] == 'i' && ln.[3] == 'p' &&
			ln.[4] == ':' && ln.[5] == '/' // skipped regex
		= [(lineno,TRegex True (ln % (6,size ln-3))):read lineno f]
	| ln.[0] == ' ' && ln.[1] == ' ' && ln.[2] == ' ' && ln.[3] == ' ' // input
		= [(lineno,TInput (ln % (4,size ln-2))):read lineno f]
	| ln.[0] == ' ' && ln.[1] == ' ' // escaped input
		= [(lineno,TInput (unescape (ln % (2,size ln-2)))):read lineno f]
	| ln.[0] == ' ' && isDigit ln.[1] && ln.[2] == ':' && ln.[3] == ' ' // match
		= [(lineno,TMatch (toInt {#ln.[1]}) (unescape (ln % (4,size ln-2)))):read lineno f]
	| isDigit ln.[0] && isDigit ln.[1] && ln.[2] == ':' && ln.[3] == ' ' // match
		= [(lineno,TMatch (toInt (ln % (0,1))) (ln % (4,size ln-2))):read lineno f]
	| ln.[0] == 'N' && ln.[1] == 'o' && ln.[2] == ' ' && ln.[3] == 'm' &&
			ln.[4] == 'a' && ln.[5] == 't' && ln.[6] == 'c' && ln.[7] == 'h' // no match
		= [(lineno,TNoMatch):read lineno f]
	| ln.[0] == '\\' // \= Expect no match; basically a comment
		= read lineno f
	| ln.[0] == 'M' && ln.[1] == 'K' && ln.[2] == ':' // mark; ignore for now
		= read lineno f
	| otherwise
		= abort ("Unrecognized line in test input: "+++ln)

// For unescape
instance * Char where (*) a b = toChar (toInt a * toInt b)

unescape :: !String -> String
unescape s
	# cs = unescape [c \\ c <-: s]
	= {c \\ c <- cs}
where
	unescape :: ![Char] -> [Char]
	unescape ['\\','\\':cs] = ['\\':unescape cs]
	unescape ['\\','x',c1,c2:cs]
		| not (isHexDigit c1)
			= abort "invalid hexadecimal escape sequence"
		| isHexDigit c2
			= [toChar ((hex c1 << 4) + hex c2):unescape cs]
			= [toChar (hex c1):unescape [c2:cs]]
	where
		hex :: !Char -> Int
		hex c
			| isDigit c
				= toInt (c-'0')
			| isUpper c
				= toInt (c-'A') + 10
				= toInt (c-'a') + 10
	unescape ['\\','n':cs] = ['\n':unescape cs]
	unescape ['\\','t':cs] = ['\t':unescape cs]
	unescape ['\\','r':cs] = ['\r':unescape cs]
	unescape ['\\','f':cs] = ['\f':unescape cs]
	unescape ['\\','a':cs] = ['\x07':unescape cs]
	unescape ['\\','b':cs] = ['\x08':unescape cs]
	unescape ['\\','e':cs] = ['\x1b':unescape cs]
	unescape ['\\',c1:cs]
		| '0' <= c1 && c1 <= '7' = case cs of
			[] -> [c1-'0']
			[c2:cs]
				| '0' <= c2 && c2 <= '7' -> case cs of
					[] -> [(c1-'0')*'\10' + c2-'0']
					[c3:cs]
						| '0' <= c3 && c3 <= '7'
							-> [(c1-'0')*'\100' + (c2-'0')*'\10' + c3-'0':unescape cs]
							-> [(c1-'0')*'\10' + c2-'0':unescape [c3:cs]]
				| otherwise -> [c1-'0':unescape [c2:cs]]
		| otherwise = abort ("unknown escape sequence '\\"+++{c1}+++"'\n")
	unescape [c:cs] = [c:unescape cs]
	unescape [] = []

escape :: !String -> String
escape s
	# sz = size s
	# escsz = escaped_size (sz-1) s 0
	| sz == escsz
		= s
		= copy (sz-1) (escsz-1) (createArray escsz '\0')
where
	copy :: !Int !Int !*String -> .String
	copy -1 _ new = new
	copy si newi new
		| s.[si] > '\x7f' || s.[si] < '\x20'
			= copy (si-1) (newi-4)
				{ new
				& [newi-3] = '\\'
				, [newi-2] = 'x'
				, [newi-1] = hex (toInt s.[si] >> 4)
				, [newi]   = hex (toInt s.[si] bitand 0x0f)
				}
			= copy (si-1) (newi-1) {new & [newi]=s.[si]}
	where
		hex i = "0123456789abcdef".[i]

	escaped_size :: !Int !String !Int -> Int
	escaped_size -1 _ n = n
	escaped_size i s n
		| s.[i] > '\x7f' || s.[i] < '\x20'
			= escaped_size (i-1) s (n+4)
			= escaped_size (i-1) s (n+1)
